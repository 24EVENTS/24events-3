package com04131.android.lib;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.Date;

public class Event implements Serializable {

    private static final long serialVersionUID = 1L;

	/*public String location, title;

	public Date startDate, startTime;

	public int id;*/

    @Expose
    public int id;

    @Expose
    public String dates;

    @Expose
    public String enddates;

    @Expose
    public String times;

    @Expose
    public String endtimes;

    @Expose
    public String title;

    @Expose
    public String venue;

    public Date startDate;

    public Date startTime;

    @Override
    public String toString() {
        return "[@Event " + id + ": " + title + " - " + venue + "]";
    }

}
