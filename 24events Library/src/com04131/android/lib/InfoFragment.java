package com04131.android.lib;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.actionbarsherlock.app.SherlockFragment;
import org.apache.commons.io.IOUtils;

public class InfoFragment extends SherlockFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.info, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            ((WebView) view.findViewById(R.id.webview))
                    .loadDataWithBaseURL(null, IOUtils.toString(getActivity().getAssets().open("info.html"), "UTF-8"),
                            "text/html", "UTF-8", null);
        } catch (Exception e) {
            Log.e(InfoFragment.class.getSimpleName(), "Error loading html", e);
        }
    }

}
